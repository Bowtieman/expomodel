var formOne = document.getElementById('formOne');
var formTwo = document.getElementById('formTwo');


//this actibate the function
var submitButton = document.getElementById('submitBtn');
submitButton.addEventListener('click', formValueOutput);

var formSwitchBtn = document.getElementById('formSwitchBtn');
//form switching
formSwitchBtn.addEventListener('click', alternateForms);

function alternateForms() {
  //switching form one
  if (formOne.classList.contains('d-none')) {
    formOne.classList.remove('d-none');
  } else {
    formOne.classList.add('d-none');
  }
  //switching form two
  if (formTwo.classList.contains('d-none')) {
    formTwo.classList.remove('d-none');
  } else {
    formTwo.classList.add('d-none');
  }
}

//the intrest table
var intrestTableOne = document.getElementById('intrestTableOne');
var intrestTableTwo = document.getElementById('intrestTableTwo');

//this is the additional tablble that you van open
var additionalTableBtn = document.getElementById('tableBtn');
additionalTableBtn.addEventListener('click', addNewTable);

function addNewTable() {
  var pageContainer = document.getElementById('pageContainer');
  var tableTwo = document.getElementById('tableTwoColum');
  if (tableTwo.style.display == 'none') {
    tableTwo.style.display = 'block';
    pageContainer.style.width = '600px'; // Resizes the new window
  } else {
    tableTwo.style.display = 'none';
  }
}

//=====================================================
//                   the chart
//=====================================================

var ctx = document.getElementById('myChart').getContext('2d');
var chartData = {
  // The type of chart we want to create
  type: 'line',

  // The data for our dataset
  data: {
    labels: [],
    datasets: [{
        label: 'My First dataset',
        borderColor: 'rgba(76, 162, 205, 1)',
        data: []
      },
      {
        label: 'My sexond dataset',
        borderColor: 'rgba(205, 60, 35, 1)',
        data: []
      }
    ]
  },

  // Configuration options go here
  options: {}
};


function formValueOutput() {
  if (formOne.classList.contains('d-none') === false) {
    // this is the numbers for the formula
    var principle = document.getElementById('principle');
    var intrest = document.getElementById('intrest');
    var compoundPerUnit = document.getElementById('compoundPerUnit');
    var time = document.getElementById('time');
    var intrestTable = document.getElementById('intrestTableOne');
    var dataYAxis = chartData.data.datasets[0].data;
  } else if (formTwo.classList.contains('d-none') === false) {
    // this is the numbers for the formula
    var principle = document.getElementById('principleTwo');
    var intrest = document.getElementById('intrestTwo');
    var compoundPerUnit = document.getElementById('compoundPerUnitTwo');
    var time = document.getElementById('timeTwo');
    var intrestTable = document.getElementById('intrestTableTwo');
    var dataYAxis = chartData.data.datasets[1].data;
  }
  expoIncrease(principle, intrest, compoundPerUnit, time, intrestTable, dataYAxis);
}

function expoIncrease(principle, intrest, compoundPerUnit, time, intrestTable, dataYAxis) {
  var a = 0;
  var totalA = Number(principle.value);
  var yearNum = 1;

  if (chartData.data.labels.length === 0) {
    for (var i = 0; i < time.value; i++) {
      //thi is the xaxis for the gtaph that will shows the years
      chartData.data.labels.push('Year ' + yearNum);
      var chart = new Chart(ctx, chartData);
      yearNum++;
    }
  } else {
    yearNum += chartData.data.labels.length;
    for (var i = chartData.data.labels.length; i < time.value; i++) {
      //thi is the xaxis for the gtaph that will shows the years
      chartData.data.labels.push('Year ' + yearNum);
      var chart = new Chart(ctx, chartData);
      yearNum++;
    }
  }

  for (var i = 0; i < time.value; i++) {
    //cerating the new row for the table
    var newTableRow = document.createElement('tr');
    var newTableHeader = document.createElement('th');
    var newTableDatum = document.createElement('td');
    //the formula calcyulation
    a = intrest.value / compoundPerUnit.value + 1;
    Math.pow(a, time.value);
    totalA *= a;
    a *= principle.value;
    //saving the currentamount to append to the new elements
    var textnodeYear = document.createTextNode(Number(i) + 1);
    newTableHeader.appendChild(textnodeYear);
    var textnodeDatum = document.createTextNode("$" + totalA.toFixed(2));
    newTableDatum.appendChild(textnodeDatum);
    dataYAxis.push(totalA.toFixed(2));
    var chart = new Chart(ctx, chartData);
    //appenig all new elements
    newTableRow.appendChild(newTableHeader);
    newTableRow.appendChild(newTableDatum);
    intrestTable.appendChild(newTableRow);
  }
}